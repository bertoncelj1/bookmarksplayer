

var videoUrlList = [];
var videoUrlListRandom = [];

function playRandomPLaylist(){
  //https://www.youtube.com/embed/?autoplay=1&playlist=9vuXvq_PGHQ,xlTLceZoJgs
  var list = "";

  videoUrlListRandom.sort(function(a, b) {
    return 0.5 - Math.random();
  });

  var maxLen = 100;
  var len = (videoUrlList.length < maxLen)? videoUrlList.length : maxLen;
  
  var firstVideoID = getYoutubeVideoId(videoUrlListRandom[0].url)
  for(var i=1; i<len-1; i++){
    list += getYoutubeVideoId(videoUrlListRandom[i].url) + ",";
  }
  list += getYoutubeVideoId(videoUrlListRandom[len-1].url);

  var PlaylistUrl = "https://www.youtube.com/embed/" + firstVideoID + "?autoplay=1&playlist=" + list;
  //var tab = chrome.tabs.create({url: PlaylistUrl});
  
  openNewBackgroundTab(PlaylistUrl);
  //var myWindow = window.open("/player.html");
  //myWindow.document.$('body').html('test');

}

function openNewBackgroundTab(url){
  /*
    var a = document.createElement("a");
    a.href = url;
    var evt = document.createEvent("MouseEvents");
    //the tenth parameter of initMouseEvent sets ctrl key
    evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0,
                                true, false, false, false, 0, null);
    a.dispatchEvent(evt);
  */  

  /*
    var popupWindow = window.open(url);
    popupWindow.blur();
    window.focus();
  */
  
    chrome.tabs.create({url: url});
}
//<iframe width="560" height="315"
//src="https://www.youtube.com/embed/Cu6xIuLJGes"
//frameborder="0" allowfullscreen></iframe>
function playVideo(urlGet){
  //var frame = $("<iframe>");
  //var src = getSrc(url);
  chrome.tabs.create({url: getSrc(urlGet)});
  return;
  frame.attr({
    "width": 560,
    "height": 325,
    "src": src,
    "frameborder": 0
  });
  $("#video").empty();
  $("#video").append(frame);

}

function getSrc(url){
  var strUrl = String(url);

  //for youtube
  if(strUrl.indexOf("www.youtube.com/watch?") != -1){
    var indexStart = strUrl.indexOf("v=");
    if(indexStart < 0)return "no v= found";
    indexStart += "v=".length;

    for(var i=indexStart; i<strUrl.length; i++){
      if(strUrl[i] == '&')break;
    }

    return "https://www.youtube.com/watch?v=" + strUrl.substring(indexStart, i);
  }

  return "no youtube found";
}

function getYoutubeVideoId(url){
    var strUrl = String(url);

    //for youtube
    if(strUrl.indexOf("www.youtube.com/watch?") != -1){
      var indexStart = strUrl.indexOf("v=");
      if(indexStart < 0)return "no v= found";
      indexStart += "v=".length;

      for(var i=indexStart; i<strUrl.length; i++){
        if(strUrl[i] == '&')break;
      }

      return strUrl.substring(indexStart, i);
    }

    return "";//no youtube found
}




$(document).ready(function(){ 
    getBookmarks();

    $("#show").click(function(){
        displayBookmarks($("#query").text().toLowerCase());
    });

    $("#add").click(function(){
        addNewbookmark();
    });

    $("#random").click(function(){
        var random = Math.floor(Math.random() * videoUrlList.length);
        playVideo(videoUrlList[random].url);
    });

    $("#randomPLaylist").click(function(){
        playRandomPLaylist();
    });

    $("#search").on('input',function(e){
      displayBookmarks($(this).val());
    });


    $("#openPlayer").click(function(){
      //var myWindow = window.open('https://www.google.si/','mywindow');
      var myWindow = window.open("player.html", "_blank");
      //myWindow.document.write("<p>This is 'MsgWindow'. I am 200px wide and 100px tall!</p>");

      myWindow.spor();
      $(myWindow.document).ready(function () {
        $(myWindow.document).contents().find('#par').html("asdasd");
      });

      myWindow.onload = function () {
        myWindow.document.getElementById('par').innerHTML = '2';
      };

      $(myWindow).load(function(){
        myWindow.$("#par").html("2");
      });
    });



});

function displayBookmarks(query){
  query = query.toLowerCase();
  var bookmarks = $('#bookmarks');
  bookmarks.empty();
  var list = $('<ul style="list-style-type:none"></ul>');
  for(var i=0; i < videoUrlList.length; i++){
    if(videoUrlList[i].name.toLowerCase().indexOf(query) > -1){
      list.append(displayBookmark(i));
    }
  }
  bookmarks.html(list);

}

function displayBookmark(i){
  var row = $("<li></li>");
  var a = $("<a></a>");
  a.attr('href', videoUrlList[i].url);
  a.html(videoUrlList[i].name);
  a.click(function() {
    playVideo(videoUrlList[i].url);
  });
  row.html(a);
  return row;
}


// Traverse the bookmark tree, and print the folder and nodes.
function getBookmarks() {
  var bookmarkTreeNodes = chrome.bookmarks.getTree(
    function(bookmarkTreeNodes) {
      bookmarksNumber = 0;
      getTreeNodes(bookmarkTreeNodes);
      videoUrlListRandom = videoUrlList.slice();
      videoUrlList.sort(function(a, b) {
        return a.name.localeCompare(b.name);
      });
      $("#bookmarksNumber").text("Number of songs: " + bookmarksNumber);
    });
}

function getTreeNodes(bookmarkNodes) {
  for (var i = 0; i < bookmarkNodes.length; i++) {
    readNode(bookmarkNodes[i]);
  }
}

function isMusicRelatedUrl(url){
	if(url.indexOf("www.youtube.com/watch?") != -1)return true;
	else if(url.indexOf("vimeo.com/") != -1)return true;
	else if(url.indexOf("soundcloud.com/") != -1)return true;
	
	return false;

}

function readNode(bookmarkNode){
  if(isMusicRelatedUrl(String(bookmarkNode.url))){

    var name = bookmarkNode.title || bookmarkNode.url || "no title or url";
    videoUrlList.push({
      name: name,
      url: bookmarkNode.url,
    });

    bookmarksNumber ++;
  }

  if (bookmarkNode.children && bookmarkNode.children.length > 0) {
    getTreeNodes(bookmarkNode.children);
  }
}
